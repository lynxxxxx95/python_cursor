#**pythonCursor**

###This is a doc for python homework tasks

####_HomeTask 1_

1) Use `git clone https://bitbucket.org/lynxxxxx95/python_cursor.git` to fetch full repository

2) Run `cd python_cursor/HW1/ && python3 HW1unittests.py` on your linux machine

**Here is an output you should get after execution both commands**

```
.............
----------------------------------------------------------------------
Ran 13 tests in 0.004s

OK
```

